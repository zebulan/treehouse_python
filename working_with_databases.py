from peewee import *
from collections import OrderedDict
import os
import sys

db = SqliteDatabase('challenges.db')


class Challenge(Model):
    name = CharField(max_length=100)
    language = CharField(max_length=100)
    steps = IntegerField(default=1)

    class Meta:
        database = db


def add_challenge():
    print('Enter your Challenge. Press ctrl+d when finished.')
    data = sys.stdin.read().strip()
    if data:
        if input('Save Challenge? [Y/N] ').lower() != 'n':
            Challenge.create(content=data)
            print('Saved successfully!')


def view_entries(search_query=None):
    entries = Challenge.select().order_by(Challenge.timestamp.desc())
    if search_query:
        entries = entries.where(Challenge.content.contains(search_query))

    for Challenge in entries:
        timestamp = Challenge.timestamp.strftime('%A %B %d, %Y %I:%M%p')
        print(timestamp)
        print('=' * len(timestamp))
        print(Challenge.content)
        print('N) next Challenge')
        print('d) delete Challenge')
        print('q) return to main menu')

        next_action = input('Action: [Nq] ').lower().strip()
        if next_action == 'q':
            break
        elif next_action == 'd':
            delete_Challenge(Challenge)


def clear():
    os.system('cls' if os.name == 'nt' else 'clear')


def delete_Challenge(challenge):
    if input('Are you sure? [yN] ').lower() == 'y':
        challenge.delete_instance()
        print('Challenge deleted!')


def search_entries():
    view_entries(input('Search query: '))


def initialize():
    db.connect()
    db.create_tables([Challenge], safe=True)


def menu_loop():
    choice = None
    while choice != 'q':
        print('Enter \'q\' to quit:')
        for k, v in menu.items():
            print('{}) {}'.format(k, v.__doc__))
        choice = input('Action: ').lower().strip()
        if choice in menu:
            menu[choice]()

menu = OrderedDict([
    ('a', add_challenge),
    ('v', view_entries),
])
