import requests
from requests.auth import HTTPBasicAuth, HTTPDigestAuth

r = requests.get('http://httpbin.org/get')
print(r.status_code)
print(r.ok)
print(r.headers)
print(r.headers.keys())
print(r.headers['Content-Length'])


# r = requests.get('https://api.github.com/events')
# timeline = r.json()
# print(timeline[0]['id'])


payload = {'content': 'I really like requests.', 'user-id': 152}
r = requests.post('http://httpbin.org/post', params=payload)
print(r.headers)
print(dir(r))
print(r.json())


payload = {'posts[]': [1, 2, 3, 4, 5, 6]}
r = requests.delete('http://httpbin.org/delete', params=payload)
print(r.json()['args'])


r = requests.get('http://httpbin.org/redirect/3')
print(r.status_code)
print(r.history)


r = requests.get('http://httpbin.org/redirect/3', allow_redirects=False)
print(r.status_code)
print(r.history)
print(r.headers)


r = requests.get('http://httpbin.org/basic-auth/user/password',
                 auth=HTTPBasicAuth('user', 'password'))  # clear text <<<
print(r.status_code)


r = requests.get('http://httpbin.org/digest-auth/auth/user/password',
                 auth=HTTPDigestAuth('user', 'password'))  # hashed <<<
print(r.status_code)
