from functools import wraps


def outer():
    number = 5

    def inner():
        print(number)

    inner()


def apply(func, x, y):
    return func(x, y)


def add(x, y):
    return x + y


def sub(x, y):
    return x - y


def close():
    x = 5

    def inner():
        print(x)
    return inner


def add_to_five(num):
    def inner():
        print(num + 5)
    return inner


def logme(func):
    import logging  # because we don't want to require users to import it
    logging.basicConfig(level=logging.DEBUG)

    def inner():
        logging.debug("Called {}".format(func.__name__))
        return func()
    return inner


@logme
def say_hello():
    print("Hello there!")


def logme_w_args(func):
    import logging  # because we don't want to require users to import it
    logging.basicConfig(level=logging.DEBUG)

    @wraps(func)
    def inner(*args, **kwargs):
        logging.debug("Called {} with args: {} and kwargs: {}".format(
            func.__name__, args, kwargs))
        return func(*args, **kwargs)
    # inner.__doc__ = func.__doc__
    # inner.__name__ = func.__name__
    return inner


@logme_w_args
def subtract(x, y, switch=False):
    return x - y if not switch else y - x

subtract(5, 2)

say_hello()  # logs the call and then prints "Hello there!"

# fifteen = add_to_five(10)
# fifteen()

# closure = close()
# closure()

# print(apply(add, 5, 5))
# print(apply(sub, 2, 8))
