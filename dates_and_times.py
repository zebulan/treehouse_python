import datetime
import pytz


dt = datetime.datetime.now()

# print(dt.strftime('%d %B %Y'))
# s = '20 August 2015'
# print(type(dt.strptime(s, '%d %B %Y')))

STARTER = datetime.datetime(2015, 10, 21, 16, 29)


def delorean(num):
    return STARTER.replace(hour=STARTER.hour + num)


def time_machine(num, string):
    if string == 'hours':
        num *= 60
    elif string == 'days':
        num *= 1440
    elif string == 'years':
        num *= 525600
    return STARTER + datetime.timedelta(minutes=num)


def timestamp_oldest(*args):
    return datetime.datetime.fromtimestamp(min(args))

moscow = datetime.timezone(datetime.timedelta(hours=4))
pacific = datetime.timezone(datetime.timedelta(hours=-8))
india = datetime.timezone(datetime.timedelta(hours=5.5))


naive = datetime.datetime(2015, 10, 21, 4, 29)
hill_valley = naive.replace(tzinfo=datetime.timezone(datetime.timedelta(hours=-8)))

################################

fmt = '%m-%d %H:%M %Z%z'
starter = datetime.datetime(2015, 10, 21, 4, 29)
local = pytz.timezone('US/Pacific').localize(starter)
pytz_string = local.strftime(fmt)

# ################################
# TIMEZONES = [
#     pytz.timezone('US/Eastern'),
#     pytz.timezone('Pacific/Auckland'),
#     pytz.timezone('Asia/Calcutta'),
#     pytz.timezone('UTC'),
#     pytz.timezone('Europe/Paris'),
#     pytz.timezone('Africa/Khartoum')
# ]
#
# fmt = '%Y-%m-%d %H:%M:%S %Z%z'
#
# while True:
#     date_input = input('When is your meeting? Please use MM/DD/YYYY HH:MM format. ')
#     try:
#         local_date = datetime.datetime.strptime(date_input, '%m/%d/%Y %H:%M')
#     except ValueError:
#         print('Invalid date/time')
#     else:
#         local_date = pytz.timezone('US/Pacific').localize(local_date)
#         utc_date = local_date.astimezone(pytz.utc)
#
#         output = [utc_date.astimezone(timezone) for timezone in TIMEZONES]
#         [print(utc_date.astimezone(t).strftime(fmt)) for t in TIMEZONES]
#         break

# print(type(pytz.utc.localize(datetime.datetime(2015, 10, 21, 23, 29))))
